#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#ifdef __cplusplus
extern "C" {
#endif

void temperature_task(void *pvParameters);
void vibration_task(void *pvParameters);
void aws_iot_task(void *param);

int wait_wifi(void);

float getTemperature();
float getHumidity();
int   getVibration();

#ifdef __cplusplus
}
#endif


#endif  // __INTERFACE_H__