#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

#include <driver/adc.h>


static const gpio_num_t adc1_gpio = CONFIG_VIBRATION_SENSOR_PIN;

static const char *TAG = "ADC1";

static int vibration = 0.0;

static adc1_channel_t getChannel(gpio_num_t gpio)
{
    adc1_channel_t channel = ADC1_CHANNEL_MAX;
    switch(gpio){
        case 32:
            channel = ADC1_CHANNEL_4;
            break;
        case 33:
            channel = ADC1_CHANNEL_5;
            break;
        case 34:
            channel = ADC1_CHANNEL_6;
            break;
        case 35:
            channel = ADC1_CHANNEL_7;
            break;
        case 36:
            channel = ADC1_CHANNEL_0;
            break;
        case 37:
            channel = ADC1_CHANNEL_1;
            break;
        case 38:
            channel = ADC1_CHANNEL_2;
            break;
        case 39:
            channel = ADC1_CHANNEL_3;
            break;
        default:
            break;
    }
    if(channel == ADC1_CHANNEL_MAX){
        ESP_LOGE(TAG, "Wrong pin selected for ADC1!!!");
    }
    else
    {
        ESP_LOGI(TAG, "returning channel %d", (int)channel);
    }
    return channel;
} 

void vibration_task(void *pvParameters)
{
    adc1_channel_t channel = getChannel(adc1_gpio);

    adc1_config_width(ADC_WIDTH_BIT_10);
    adc1_config_channel_atten(channel,ADC_ATTEN_DB_11);

    while (1)
    {
        ESP_LOGD(TAG, "Stack remaining for task '%s' is %d bytes", pcTaskGetTaskName(NULL), uxTaskGetStackHighWaterMark(NULL));
        vibration = adc1_get_raw(channel);

        ESP_LOGI(TAG, "Vibration: %d", vibration);

        vTaskDelay(CONFIG_VIBRATION_PERIOD_MS / portTICK_RATE_MS);
    }
}

int getVibration()
{
    return vibration;
}

