#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

#include <dht.h>

static const dht_sensor_type_t sensor_type = DHT_TYPE_AM2301;
static const gpio_num_t dht_gpio = CONFIG_TEMP_SENSOR_PIN;

static const char *TAG = "DHT";

static float temperature = 0.0;
static float humidity = 0.0;

void temperature_task(void *pvParameters)
{
    // DHT sensors that come mounted on a PCB generally have
    // pull-up resistors on the data pin.  It is recommended
    // to provide an external pull-up resistor otherwise...

    gpio_set_pull_mode(dht_gpio, GPIO_PULLUP_ONLY);

    while (1)
    {
        ESP_LOGD(TAG, "Stack remaining for task '%s' is %d bytes", pcTaskGetTaskName(NULL), uxTaskGetStackHighWaterMark(NULL));
        if (dht_read_float_data(sensor_type, dht_gpio, &humidity, &temperature) == ESP_OK)
        {
            ESP_LOGI(TAG, "Humidity: %.2f%% Temp: %.2fC", humidity, temperature);
        }
        else
            ESP_LOGE(TAG, "Could not read data from sensor");

        // If you read the sensor data too often, it will heat up
        // http://www.kandrsmith.org/RJS/Misc/Hygrometers/dht_sht_how_fast.html
        vTaskDelay(CONFIG_TEMP_PERIOD_MS / portTICK_RATE_MS);
    }
}

float getTemperature()
{
    return temperature;
}

float getHumidity()
{
    return humidity;
}
