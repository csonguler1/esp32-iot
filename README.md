# ESP-AWS-IoT Collector

This project uses AWS-ESP-IoT framework which enables AWS IoT cloud connectivity with ESP32 based platforms using [AWS IoT Device Embedded SDK](https://github.com/aws/aws-iot-device-sdk-embedded-C).
Instead of the examples folder in the official one, this one has a collector folder. This collector app read temperature and humidity values and sends them to AWS IoT.

## Getting Started

- Please clone this repository using,
    ```
    git clone https://gitlab.com/csonguler1/esp32-iot
    ```
- Please refer to https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html for setting ESP-IDF
  - ESP-IDF can be downloaded from https://github.com/espressif/esp-idf/
  - ESP-IDF v3.1 and above is recommended version
- Go to collector folder and build.
